﻿using System;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Worms.Behavior;
using Worms.Hosted;
using Worms.Model;
using Worms.Service.Food.Generation;
using Worms.Service.Report.Generation;
using Worms.Service.Strategy;

namespace Worms
{
    public static class Program
    {
        private const string DbConnectionString = @"Server=localhost;Port=5432;Database=food;Username=food;Password=food";
        
        private const string WebStrategyIP = "localhost";
        private const string WebStrategyPort = "5000";

        private static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<SimulationService>();
                    services.AddDbContext<DatabaseContext>(op => op.UseNpgsql(DbConnectionString));
                    services.AddSingleton<World>();
                    services.AddScoped<IReportGenerator, ConsoleReportGenerator>();
                    services.AddScoped<AbstractStrategy>(sp => new RemoteHostStrategy(WebStrategyIP, WebStrategyPort));
                    //services.AddScoped<AbstractStrategy,FirstStrategy>();
                    if (args.Length == 0)
                    {
                        services.AddScoped<IFoodGenerator, NormalFoodGenerator>();
                    }
                    else
                    {
                        var behaviorName = args[0];
                        services.AddScoped<IFoodGenerator>(sp =>
                            new FromBehaviorFoodGenerator(sp.GetService<DatabaseContext>(),
                                sp.GetService<World>(),
                                behaviorName));
                    }
                });
    }
}