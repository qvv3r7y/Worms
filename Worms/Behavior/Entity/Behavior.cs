namespace Worms.Behavior.Entity
{
    public class Behavior
    {
        public int BehaviorId { get; set; }
        public string Name { get; set; }
    }
}