namespace Worms.Behavior.Entity
{
    public class Food
    {
        public int FoodId { get; set; }
        public int BehaviorId { get; set; }
        public int Step { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}