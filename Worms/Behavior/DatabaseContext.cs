using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Worms.Behavior.Entity;

namespace Worms.Behavior
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Entity.Behavior> Behaviors { get; set; }
        public DbSet<Food> Foods { get; set; }

        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BehaviorConfiguration());
            modelBuilder.ApplyConfiguration(new FoodConfiguration());
        }

        public List<Food> GetFoodsFromBehavior(string behaviorName)
        {
            var behavior = Behaviors.FirstOrDefault(b => b.Name == behaviorName);
            if (behavior == null) return null;
            var foods = Foods
                .Where(f => f.BehaviorId == behavior.BehaviorId)
                .OrderBy(f => f.Step)
                .ToList();
            return foods;
        }

        public class BehaviorConfiguration : IEntityTypeConfiguration<Entity.Behavior>
        {
            public void Configure(EntityTypeBuilder<Entity.Behavior> builder)
            {
                builder.ToTable("behavior");
                builder.Property(b => b.BehaviorId).HasColumnName("behavior_id");
                builder.Property(b => b.Name).HasColumnName("name").IsRequired().HasMaxLength(128);
            }
        }

        public class FoodConfiguration : IEntityTypeConfiguration<Food>
        {
            public void Configure(EntityTypeBuilder<Food> builder)
            {
                builder.ToTable("food");
                builder.Property(f => f.FoodId).HasColumnName("food_id");
                builder.Property(f => f.BehaviorId).HasColumnName("behavior_id").IsRequired();
                builder.Property(f => f.Step).HasColumnName("step").IsRequired();
                builder.Property(f => f.X).HasColumnName("x").IsRequired();
                builder.Property(f => f.Y).HasColumnName("y").IsRequired();
            }
        }
    }
}