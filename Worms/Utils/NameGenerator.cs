using System.IO;

namespace Worms.Utils
{
    public static class NameGenerator
    {
        private const string PathWormNames = "./worm_names.txt";
        private static readonly string[] Names;
        private static int _countName;

        static NameGenerator()
        {
            using var streamReader = new StreamReader(PathWormNames);
            Names = streamReader.ReadToEnd().Split("\n");
        }

        public static string GenerateName()
        {
            var name = Names[_countName++ % Names.Length];
            return name;
        }
    }
}