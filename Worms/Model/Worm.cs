using System;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using Worms.ModelConvert;
using Worms.Service.Strategy;
using Worms.Utils;

namespace Worms.Model
{
    public class Worm
    {
        private string _name;
        private int _health;
        private (int x, int y) _coords;

        private readonly World _world;
        private readonly AbstractStrategy _abstractStrategy;

        public Worm(AbstractStrategy abstractStrategy, World world, int x = 0, int y = 0)
        {
            _health = 10;
            _coords.x = x;
            _coords.y = y;
            _world = world;
            _name = NameGenerator.GenerateName();
            //_abstractStrategy = abstractStrategy.GetIndividualStrategy(_world, this);
            _abstractStrategy = abstractStrategy;
        }

        public void Step()
        {
            if (_world.TryEatFood(_coords))
            {
                ImproveHealth();
            }

            _abstractStrategy.BuildRoute(_world, this);
            _health--;
        }

        public void Move(Direction direction)
        {
            var newCoords = ComputeNewCoords(direction);
            if (_world.CheckCellForWorm(newCoords) != null) return;
            if (_world.TryEatFood(newCoords))
            {
                ImproveHealth();
            }

            _coords = newCoords;
        }

        public void Division(Direction direction)
        {
            if (_health <= 10) return;
            var newCoords = ComputeNewCoords(direction);
            if (_world.CheckCellForWorm(newCoords) != null) return;
            if (_world.CheckCellForFood(newCoords) != null) return;

            _health -= 10;
            var child = new Worm(_abstractStrategy, _world, newCoords.x, newCoords.y);
            _world.AddNewWorm(child);
        }

        public (int x, int y) ComputeNewCoords(Direction direction)
        {
            var newCoords = _coords;
            switch (direction)
            {
                case Direction.Left:
                    newCoords.x--;
                    break;
                case Direction.Right:
                    newCoords.x++;
                    break;
                case Direction.Down:
                    newCoords.y--;
                    break;
                case Direction.Up:
                    newCoords.y++;
                    break;
            }

            return newCoords;
        }

        private void ImproveHealth()
        {
            _health += 10;
        }

        public (int x, int y) Coords => _coords;

        public int GetHealth()
        {
            return _health;
        }

        public bool IsDead()
        {
            return _health <= 0;
        }

        public string GetName()
        {
            return _name;
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public int Health
        {
            get => _health;
            set => _health = value;
        }
    }
}