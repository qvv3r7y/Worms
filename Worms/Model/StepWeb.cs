using System.Text.Json.Serialization;
using Worms.Service.Strategy;

namespace Worms.Model
{
    public class StepWeb
    {
        public string direction { get; set; }
        public bool split { get; set; }
        
    }
}