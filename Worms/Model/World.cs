using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Worms.Model
{
    public class World
    {
        
        private readonly List<Worm> _worms = new();
        private readonly Dictionary<(int x, int y), Food> _foods = new();
        private int _stepCounter = 0;

        public Worm CheckCellForWorm((int x, int y) coords)
        {
            var (x, y) = coords;
            return _worms.FirstOrDefault(worm => worm.Coords.x == x && worm.Coords.y == y);
        }

        public Food CheckCellForFood((int x, int y) coords)
        {
            _foods.TryGetValue(coords, out var food);
            return food;
        }

        public bool TryEatFood((int x, int y) coords)
        {
            return _foods.Remove(coords);
        }

        public void DecreaseFreshnessAndRemoveExpiredFood()
        {
            var foods = GetFoods();
            foreach (var food in foods)
            {
                food.DecreaseFreshness();
                if (food.IsFresh() == false)
                {
                    _foods.Remove(food.Coords);
                }
            }
        }

        public void RemoveDeadWorms()
        {
            _worms.RemoveAll(worm => worm.IsDead());
        }

        public void IncreaseStepCounter()
        {
            _stepCounter++;
        }

        public void AddNewWorm(Worm worm)
        {
            _worms.Add(worm);
        }

        public bool TryAddNewFood(Food food)
        {
            return _foods.TryAdd(food.Coords, food);
        }

        public List<Worm> GetWorms()
        {
            return _worms;
        }

        public List<Food> GetFoods()
        {
            return new List<Food>(_foods.Values);
        }

        public int GetStepCounter()
        {
            return _stepCounter;
        }
    }
}