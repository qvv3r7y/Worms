using System.Text.Json.Serialization;

namespace Worms.Model
{
    public class Food
    {
        public int Freshness { get; set; }
        private (int x, int y) _coords;


        public Food(int x, int y)
        {
            Freshness = 10;
            _coords.x = x;
            _coords.y = y;
        }

        public void DecreaseFreshness()
        {
            Freshness--;
        }

        public bool IsFresh()
        {
            return Freshness > 0;
        }

        public (int x, int y) Coords => _coords;
    }
}