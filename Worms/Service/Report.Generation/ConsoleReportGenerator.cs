using System;
using System.IO;
using System.Text;
using Worms.Model;

namespace Worms.Service.Report.Generation
{
    public class ConsoleReportGenerator : IReportGenerator
    {
        private readonly World _world;
        
        public ConsoleReportGenerator(World world)
        {
            _world = world;
        }

        public void Report()
        {
            Console.WriteLine(GetStepReport());
        }
        
        public void PrintWormsCount()
        {
            Console.WriteLine("Worms alive: " + _world.GetWorms().Count);
        }
        
        private string GetStepReport()
        {
            var reportSB = new StringBuilder(256);
            var wormSB = new StringBuilder(128);
            var foodSB = new StringBuilder(128);

            foreach (var worm in _world.GetWorms())
            {
                wormSB.AppendFormat($"{worm.GetName()}-{worm.GetHealth()} {worm.Coords}, ");
            }

            if (wormSB.Length != 0) wormSB.Remove(wormSB.Length - 2, 2);

            foreach (var food in _world.GetFoods())
            {
                foodSB.AppendFormat($"{food.Coords}, ");
            }

            if (foodSB.Length != 0) foodSB.Remove(foodSB.Length - 2, 2);

            reportSB.Append("Worms:[").Append(wormSB).Append("],").Append("Food:[").Append(foodSB).Append(']');
            return reportSB.ToString();
        }
    }
}