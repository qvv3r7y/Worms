using System.IO;
using System.Text;
using Worms.Model;

namespace Worms.Service.Report.Generation
{
    public class FileReportGenerator : IReportGenerator
    {
        private const string DirPath = "../../../";
        private readonly World _world;
        private readonly string _fileName;

        public FileReportGenerator(World world, string fileName = "game.txt")
        {
            _world = world;
            _fileName = fileName;
            File.Delete(DirPath + fileName);
        }
        
        public void Report()
        {
            using var outFile = new StreamWriter(DirPath + _fileName, true);
            outFile.WriteLine(GetStepReport());
            outFile.Flush();
        }

        public void PrintWormsCount()
        {
            using var outFile = new StreamWriter(DirPath + _fileName, true);
            outFile.WriteLine("Worms alive: " + _world.GetWorms().Count);
            outFile.Flush();
        }

        private string GetStepReport()
        {
            var reportSB = new StringBuilder(256);
            var wormSB = new StringBuilder(128);
            var foodSB = new StringBuilder(128);

            foreach (var worm in _world.GetWorms())
            {
                wormSB.AppendFormat($"{worm.GetName()}-{worm.GetHealth()} {worm.Coords}, ");
            }

            if (wormSB.Length != 0) wormSB.Remove(wormSB.Length - 2, 2);

            foreach (var food in _world.GetFoods())
            {
                foodSB.AppendFormat($"{food.Coords}, ");
            }

            if (foodSB.Length != 0) foodSB.Remove(foodSB.Length - 2, 2);

            reportSB.Append("Worms:[").Append(wormSB).Append("],").Append("Food:[").Append(foodSB).Append(']');
            return reportSB.ToString();
        }
    }
}