namespace Worms.Service.Report.Generation
{
    public interface IReportGenerator
    {
        void Report();
        void PrintWormsCount();
    }
}