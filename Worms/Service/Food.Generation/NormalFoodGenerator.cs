using System;
using Worms.Model;
using Worms.Utils;

namespace Worms.Service.Food.Generation
{
    public class NormalFoodGenerator : IFoodGenerator
    {
        private readonly World _world;
        
        public NormalFoodGenerator(World world)
        {
            _world = world;
        }
        
        public void GenerateFood()
        {
            var random = new Random();
            while (true)
            {
                int x = random.NextNormal();
                int y = random.NextNormal();
                var food = new Model.Food(x, y);
                if (_world.TryAddNewFood(food)) break;
            }
        }
    }
}