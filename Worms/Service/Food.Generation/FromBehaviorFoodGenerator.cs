using System.Collections.Generic;
using Worms.Behavior;
using Worms.Model;

namespace Worms.Service.Food.Generation
{
    public class FromBehaviorFoodGenerator : IFoodGenerator
    {
        private readonly List<Behavior.Entity.Food> _foods;
        private readonly World _world;
        private int step = 0;

        public FromBehaviorFoodGenerator(DatabaseContext dbContext, World world, string behaviorName)
        {
            _foods = dbContext.GetFoodsFromBehavior(behaviorName);
            _world = world;
        }

        public void GenerateFood()
        {
            var food = new Model.Food(_foods[step].X, _foods[step].Y);
            _world.TryAddNewFood(food);
            ++step;
        }
    }
}