namespace Worms.Service.Food.Generation
{
    public interface IFoodGenerator
    {
        void GenerateFood();
    }
}