using System;
using System.Linq;
using Worms.Model;

namespace Worms.Service.Strategy
{
    public class UpStrategy : AbstractStrategy
    {
        private World _world;
        private Worm _worm;

        public override void BuildRoute(World world, Worm worm)
        {
            _world = world;
            _worm = worm;

            var x = _worm.Coords.x;
            var y = _worm.Coords.y;

            if (_worm.GetHealth() <= 10)
            {
                var (xNearest, yNearest) = FindNearestFood(world, worm);
                GoToNearestFood(x, y, xNearest, yNearest, world, worm);
            }
            else if (_worm.GetHealth() > 17 && _world.GetWorms().Count == 1)
            {
                worm.Division(Direction.Down);
            }
            else if (_world.GetStepCounter() > 90)
            {
                switch (_world.GetStepCounter() % 2)
                {
                    case 0:
                        worm.Division(Direction.Left);
                        break;
                    case 1:
                        worm.Division(Direction.Right);
                        break;
                    case 2:
                        worm.Division(Direction.Up);
                        break;
                    case 3:
                        worm.Division(Direction.Down);
                        break;
                }
            }
            else
            {
                var (xNearest, yNearest) = FindNearestFoodUp();
                GoToNearestFood(x, y, xNearest, yNearest, world, worm);
            }
        }

        private (int, int) FindNearestFoodUp()
        {
            var nearestFood = (0, 0);
            var nearestRoute = int.MaxValue;
            foreach (var food in _world.GetFoods().Where(f => f.Coords.y >= 0))
            {
                var route = Math.Abs(food.Coords.x - _worm.Coords.x) + Math.Abs(food.Coords.y - _worm.Coords.y);
                if (route < nearestRoute && food.Freshness >= route)
                {
                    nearestRoute = route;
                    nearestFood = food.Coords;
                }
            }

            return nearestFood;
        }
    }
}