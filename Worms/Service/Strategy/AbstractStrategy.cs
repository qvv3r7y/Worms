using System;
using Worms.Model;

namespace Worms.Service.Strategy
{
    public abstract class AbstractStrategy
    {
        public abstract void BuildRoute(World world, Worm worm);

        public AbstractStrategy GetIndividualStrategy(World world, Worm worm)
        {
            var wormsNum = world.GetWorms().Count;
            return wormsNum switch
            {
                0 => new UpStrategy(),
                1 => new DownStrategy(),
                _ => new StupidStrategy()
            };
        }

        protected void GoToNearestFood(int x, int y, int xNearest, int yNearest, World world, Worm worm)
        {
            Direction xSuggestion = 0;
            Direction ySuggestion = 0;

            if (x > xNearest) xSuggestion = Direction.Left;
            else if (x < xNearest) xSuggestion = Direction.Right;

            if (y > yNearest) ySuggestion = Direction.Down;
            else if (y < yNearest) ySuggestion = Direction.Up;

            if (x != xNearest && world.CheckCellForWorm(worm.ComputeNewCoords(xSuggestion)) == null)
            {
                worm.Move(xSuggestion);
            }
            else if (y != yNearest && world.CheckCellForWorm(worm.ComputeNewCoords(ySuggestion)) == null)
            {
                worm.Move(ySuggestion);
            }
        }

        protected (int x, int y) FindNearestFood(World world, Worm worm)
        {
            var nearestFood = (0, 0);
            var nearestRoute = int.MaxValue;
            foreach (var food in world.GetFoods())
            {
                var route = Math.Abs(food.Coords.x - worm.Coords.x) + Math.Abs(food.Coords.y - worm.Coords.y);
                if (route < nearestRoute && food.Freshness >= route)
                {
                    nearestRoute = route;
                    nearestFood = food.Coords;
                }
            }

            return nearestFood;
        }
    }
}