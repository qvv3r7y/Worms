using System;
using Worms.Model;

namespace Worms.Service.Strategy
{
    public class SecondAbstractStrategy : AbstractStrategy
    {
        private World _world;
        private Worm _worm;

        public override void BuildRoute(World world, Worm worm)
        {
            _world = world;
            _worm = worm;
            var (xNearest, yNearest) = FindNearestFood(world, worm);

            var x = _worm.Coords.x;
            var y = _worm.Coords.y;

            GoToNearestFood(x, y, xNearest, yNearest, world, worm);
        }
    }
}