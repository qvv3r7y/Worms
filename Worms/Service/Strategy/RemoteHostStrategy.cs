using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using Worms.Model;
using Worms.ModelConvert;
using Worm = Worms.Model.Worm;

namespace Worms.Service.Strategy
{
    public class RemoteHostStrategy : AbstractStrategy
    {
        private readonly string _hostUri;

        public RemoteHostStrategy(string ip, string port) : base()
        {
            _hostUri = $"http://{ip}:{port}";
        }

        public override void BuildRoute(World world, Worm worm)
        {
            var jsonBody = JsonSerializer.Serialize(Converter.Old2New(world));
            
            var content = new StringContent(jsonBody, Encoding.UTF8, "application/json");

            var wormName = worm.GetName();
            Console.WriteLine(world.GetStepCounter());
            var requestUri = $"{_hostUri}/{wormName}/getAction?step={world.GetStepCounter()}";

            StepWeb step = GetNextStepWeb(requestUri, content);
            Step2Command(step, worm);
        }


        private static StepWeb GetNextStepWeb(string requestUri, HttpContent requestBody)
        {
            var webHostHandler = new HttpClientHandler();
            webHostHandler.ServerCertificateCustomValidationCallback = (message, certificate2, arg3, arg4) => true;
            using var webHost = new HttpClient(webHostHandler);
            var task = webHost.PostAsync(requestUri, requestBody);
            task.Wait();
            var responseMessage = task.Result;
            var step = responseMessage.Content.ReadAsStringAsync().Result;
            return JsonSerializer.Deserialize<StepWeb>(step);
        }

        private void Step2Command(StepWeb step, Worm worm)
        {
            if (step.split == false)
            {
                switch (step.direction)
                {
                    case "Up":
                        worm.Move(Direction.Up);
                        break;
                    case "Down":
                        worm.Move(Direction.Down);
                        break;
                    case "Left":
                        worm.Move(Direction.Left);
                        break;
                    case "Right":
                        worm.Move(Direction.Right);
                        break;
                }
            }
            else
            {
                switch (step.direction)
                {
                    case "Up":
                        worm.Division(Direction.Up);
                        break;
                    case "Down":
                        worm.Division(Direction.Down);
                        break;
                    case "Left":
                        worm.Division(Direction.Left);
                        break;
                    case "Right":
                        worm.Division(Direction.Right);
                        break;
                }
            }
        }
    }
}