using Worms.Model;

namespace Worms.Service.Strategy
{
    public class FirstStrategy : AbstractStrategy
    {
        public override void BuildRoute(World world, Worm worm)
        {
            var x = worm.Coords.x;
            var y = worm.Coords.y;
            switch (x)
            {
                case < 3 when y == 0:
                    worm.Move(Direction.Right);
                    break;
                case 3 when y < 3:
                    worm.Move(Direction.Up);
                    break;
                case > 0 when y == 3:
                    worm.Move(Direction.Left);
                    break;
                case 0 when y > 0:
                    worm.Move(Direction.Down);
                    break;
            }
        }
        
    }
}