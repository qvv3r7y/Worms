using Worms.Model;

namespace Worms.Service.Strategy
{
    public class StupidStrategy : AbstractStrategy
    {
        public override void BuildRoute(World world, Worm worm)
        {
            switch (world.GetStepCounter() % 2)
            {
                case 0: worm.Move(Direction.Right);
                    break;
                case 1: worm.Move(Direction.Left);
                    break;
            }
        }
    }
}