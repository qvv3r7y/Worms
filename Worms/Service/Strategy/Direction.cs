namespace Worms.Service.Strategy
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}