using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Worms.Model;
using Worms.Service.Food.Generation;
using Worms.Service.Report.Generation;
using Worms.Service.Strategy;

namespace Worms.Hosted
{
    public class SimulationService : IHostedService
    {
        private const int StepsUntilGameOver = 100;

        private readonly World _world;
        private readonly IReportGenerator _reportGenerator;
        private readonly IFoodGenerator _foodGenerator;
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly AbstractStrategy _abstractStrategy;

        public SimulationService(IHostApplicationLifetime appLifetime,IReportGenerator reportGenerator, IFoodGenerator foodGenerator, AbstractStrategy abstractStrategy, World world)
        {
            _appLifetime = appLifetime;
            _world = world;
            _reportGenerator = reportGenerator;
            _foodGenerator = foodGenerator;
            _abstractStrategy = abstractStrategy;
        }

        private void InitWorld()
        {
            var worm = new Worm(_abstractStrategy, _world);
            _world.AddNewWorm(worm);
        }

        private void StartSimulation()
        {
            while (_world.GetStepCounter() != StepsUntilGameOver)
            {
                NextIteration();
                //Thread.Sleep(10);
            }

            _reportGenerator.PrintWormsCount();
        }

        private void NextIteration()
        {
            _world.IncreaseStepCounter();
            _foodGenerator.GenerateFood();
            Poll();

            _world.DecreaseFreshnessAndRemoveExpiredFood();
            _world.RemoveDeadWorms();

            _reportGenerator.Report();
        }

        private void Poll()
        {
            var worms = new List<Worm>(_world.GetWorms());
            foreach (var worm in worms)
            {
                worm.Step();
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            InitWorld();
            StartSimulation();
            _appLifetime.StopApplication();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
        
    }
}