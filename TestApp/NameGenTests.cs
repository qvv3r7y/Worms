using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Worms.Utils;

namespace TestApp
{
    [TestFixture]
    public class NameGenTests
    {
        [Test]
        public void DuplicateNames()
        {
            var names = new List<string>();
            for (var i = 0; i < 80; i++)
            {
                names.Add(NameGenerator.GenerateName());
            }

            var anyDuplicate = names.GroupBy(x => x)
                .Where(y => y.Count() > 1)
                .Select(k => k.Key)
                .ToList();

            Assert.IsEmpty(anyDuplicate);
        }
    }
}