using System.Linq;
using BehaviorWorldGenerator;
using BehaviorWorldGenerator.Food.Generation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NUnit.Framework;


namespace TestApp
{
    [TestFixture]
    public class BehaviorGenerator
    {
        private readonly DbContextOptions<DatabaseContext> _dbContext;

        public BehaviorGenerator()
        {
            _dbContext = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase("food")
                .Options;
        }

        [Test]
        public void GenerateFoods()
        {
            var behaviorName = "hello";
            var generator = new NormalFoodGenerator();
            using var dbContext = new DatabaseContext(_dbContext);

            Assert.AreEqual(null, dbContext.Behaviors.FirstOrDefault());
            Assert.AreEqual(null, dbContext.Foods.FirstOrDefault());

            CreateHostBuilder(new[] {behaviorName}, dbContext, generator).Build().Run();

            Assert.AreEqual(1, dbContext.Behaviors.FirstOrDefault()!.BehaviorId);
            Assert.AreEqual(100, dbContext.Foods.Count());
        }

        private static IHostBuilder CreateHostBuilder(string[] args, DatabaseContext context, IFoodGenerator generator)
        {
            var behaviorName = args[0];

            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService(sp =>
                        new BehaviorService(
                            sp.GetService<IHostApplicationLifetime>(),
                            context,
                            generator,
                            behaviorName));
                });
        }
    }
}