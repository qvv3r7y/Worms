using NUnit.Framework;
using Worms.Model;
using Worms.Service.Strategy;

namespace TestApp
{
    [TestFixture]
    public class StrategyTests
    {
        [Test]
        public void NearestFoodMove()
        {
            var world = new World();
            var worm = new Worm(new SecondAbstractStrategy(), world);

            world.TryAddNewFood(new Food(3, 3));
            world.TryAddNewFood(new Food(3, -3));
            world.TryAddNewFood(new Food(-3, 3));
            world.TryAddNewFood(new Food(-3, -3));
            world.TryAddNewFood(new Food(1, 4));

            for (var i = 0; i < 5; i++)
            {
                worm.Step();
            }

            Assert.AreEqual(worm.Coords, (1, 4));
        }
    }
}