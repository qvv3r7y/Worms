using NUnit.Framework;
using Worms.Model;
using Worms.Service.Strategy;

namespace TestApp
{
    [TestFixture]
    public class FoodGenTests
    {
        private World _world;
        private Worm _worm;

        [SetUp]
        public void Setup()
        {
            _world = new World();
            _worm = new Worm(new SecondAbstractStrategy(), _world);
            _world.AddNewWorm(_worm);
        }

        [Test]
        public void OnWorm()
        {
            _world.TryAddNewFood(new Food(0, 0));
            _worm.Step();
            
            Assert.IsEmpty(_world.GetFoods());
            Assert.AreEqual(_worm.GetHealth(), 19);
        }

        [Test]
        public void Collision()
        {
            Assert.True(_world.TryAddNewFood(new Food(5, 9)));
            Assert.False(_world.TryAddNewFood(new Food(5, 9)));
        }
    }
}