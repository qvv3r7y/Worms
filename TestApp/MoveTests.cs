using NUnit.Framework;
using Moq;
using Worms.Model;
using Worms.Service.Strategy;

namespace TestApp
{
    [TestFixture]
    public class MoveTests
    {
        private World _world;
        private Worm _worm;
        
        [SetUp]
        public void Setup()
        {
            _world = new World();
            _worm = new Worm(new SecondAbstractStrategy(), _world);
            _world.AddNewWorm(_worm);
        }

        [Test]
        public void EmptyCell()
        {
            _worm.Move(Direction.Down);
            Assert.AreEqual(_worm.Coords, (0,-1));
        }

        [Test]
        public void FoodCell()
        {
            _world.TryAddNewFood(new Food(1, 1));
            _worm.Move(Direction.Up);
            _worm.Move(Direction.Right);
            Assert.AreEqual(_worm.Coords, (1,1));
            Assert.AreEqual(_worm.GetHealth(), 20);
        }

        [Test]
        public void TakenCell()
        {
            _world.AddNewWorm(new Worm(new FirstStrategy(), _world, -1,0));
            _worm.Move(Direction.Left);
            Assert.AreEqual(_worm.Coords, (0,0));
        }
        
    }
}