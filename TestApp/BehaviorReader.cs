using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Worms.Behavior;
using Worms.Behavior.Entity;

namespace TestApp
{
    [TestFixture]
    public class BehaviorReader
    {
        private readonly DbContextOptions<DatabaseContext> _dbContext;

        public BehaviorReader()
        {
            _dbContext = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase("food")
                .Options;
        }

        [Test]
        public void ReadBehavior()
        {
            using var context = new DatabaseContext(_dbContext);
            const string behaviorName = "Test";
            Assert.IsNull(context.GetFoodsFromBehavior(behaviorName));

            var behavior = new Behavior() {Name = behaviorName};
            context.Behaviors.Add(behavior);
            context.SaveChanges();

            var foods = new List<Food>();
            for (var i = 1; i <= 100; i++)
            {
                foods.Add(new Food()
                {
                    BehaviorId = behavior.BehaviorId,
                    Step = i,
                    X = new Random().Next(),
                    Y = new Random().Next()
                });
            }

            context.Foods.AddRange(foods);
            context.SaveChanges();

            Assert.AreEqual(100, context.GetFoodsFromBehavior(behaviorName).Count);
        }
    }
}