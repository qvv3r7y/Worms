using NUnit.Framework;
using Worms.Model;
using Worms.Service.Strategy;

namespace TestApp
{
    [TestFixture]
    public class DivisionTests
    {
        private World _world;
        private Worm _worm;

        [SetUp]
        public void Setup()
        {
            _world = new World();
            _worm = new Worm(new FirstStrategy(), _world);
            _world.AddNewWorm(_worm);
        }

        [Test]
        public void SuccessDivision()
        {
            _world.TryAddNewFood(new Food(1, 0));
            _worm.Move(Direction.Right);
            _worm.Division(Direction.Left);
            Assert.True(_world.GetWorms().Count == 2);
            Assert.AreEqual(_worm.GetHealth(), 10);
        }

        [Test]
        public void FailureDivisionLowHealth()
        {
            _worm.Division(Direction.Left);
            Assert.True(_world.GetWorms().Count == 1);
            Assert.AreEqual(_worm.GetHealth(), 10);
        }

        [Test]
        public void FailureDivisionTakenCell()
        {
            _world.AddNewWorm(new Worm(new FirstStrategy(), _world, -1, -1));
            _world.TryAddNewFood(new Food(-1, 0));
            _worm.Move(Direction.Left);

            _worm.Division(Direction.Down);
            Assert.True(_world.GetWorms().Count == 2);
            Assert.AreEqual(_worm.GetHealth(), 20);
        }
    }
}