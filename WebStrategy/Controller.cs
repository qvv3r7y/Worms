using System;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using WebStrategy.Model;
using WebStrategy.ModelConvert;
using WebStrategy.Strategy;
using AbstractStrategy = WebStrategy.Strategy.AbstractStrategy;
using Worm = WebStrategy.Model.Worm;

namespace WebStrategy
{
    [ApiController]
    [Route("[controller]")]
    public class Controller : ControllerBase
    {
        private readonly AbstractStrategy _wormStrategy;

        public Controller(AbstractStrategy strategy)
        {
            _wormStrategy = strategy;
        }

        [HttpPost("{name}/getAction/{step?}/{run?}")]
        public StepWeb GetNextStep(string name, [FromBody] Field field, [FromQuery]int step, int run)
        {
            Console.WriteLine(JsonSerializer.Serialize(field));
            World world = Converter.New2Old(field,step);
            
            Worm worm = world.GetWorms().Find(w => w.GetName().Equals(name));
            
            var resp = _wormStrategy.BuildRoute(world, worm);
            if (resp == null)
            {
                Console.WriteLine(name);
            }
            var rep = _wormStrategy.BuildRoute(world, worm);
            Console.WriteLine(step);

            return resp;
        }
    }
}