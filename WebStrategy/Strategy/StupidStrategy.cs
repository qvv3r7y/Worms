using WebStrategy.Model;

namespace WebStrategy.Strategy
{
    public class StupidStrategy : WebStrategy.Strategy.AbstractStrategy
    {
        public override StepWeb BuildRoute(World world, Worm worm)
        {
            switch (world.GetStepCounter() % 2)
            {
                case 0: return new StepWeb(Direction.Right,false);
                    break;
                case 1: return new StepWeb(Direction.Left,false);
                    break;
            }

            return null;
        }
    }
}