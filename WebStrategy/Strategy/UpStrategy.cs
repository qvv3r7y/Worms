using System;
using System.Linq;
using WebStrategy.Model;

namespace WebStrategy.Strategy
{
    public class UpStrategy : WebStrategy.Strategy.AbstractStrategy
    {
        private World _world;
        private Worm _worm;

        public override StepWeb BuildRoute(World world, Worm worm)
        {
            _world = world;
            _worm = worm;

            var x = _worm.Coords.x;
            var y = _worm.Coords.y;

            if (_worm.GetHealth() <= 10)
            {
                var (xNearest, yNearest) = FindNearestFood(world, worm);
                return GoToNearestFood(x, y, xNearest, yNearest, world, worm);
            }
            else if (_worm.GetHealth() > 17 && _world.GetWorms().Count == 1)
            {
                return new StepWeb(Direction.Down, true);
                worm.Division(Direction.Down);
            }
            else if (_world.GetStepCounter() > 90)
            {
                switch (_world.GetStepCounter() % 2)
                {
                    case 0:
                        return new StepWeb(Direction.Left, true);
                        worm.Division(Direction.Left);
                        break;
                    case 1:
                        return new StepWeb(Direction.Right, true);
                        worm.Division(Direction.Right);
                        break;
                }
            }
            else
            {
                var (xNearest, yNearest) = FindNearestFoodUp();
                return GoToNearestFood(x, y, xNearest, yNearest, world, worm);
            }
           
            return null;
        }

        private (int, int) FindNearestFoodUp()
        {
            var nearestFood = (0, 20);
            var nearestRoute = int.MaxValue;
            foreach (var food in _world.GetFoods().Where(f => f.Coords.y >= 0))
            {
                var route = Math.Abs(food.Coords.x - _worm.Coords.x) + Math.Abs(food.Coords.y - _worm.Coords.y);
                if (route < nearestRoute && food.Freshness >= route)
                {
                    nearestRoute = route;
                    nearestFood = food.Coords;
                }
            }

            return nearestFood;
        }
    }
}