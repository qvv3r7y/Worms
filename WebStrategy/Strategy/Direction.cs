namespace WebStrategy.Strategy
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}