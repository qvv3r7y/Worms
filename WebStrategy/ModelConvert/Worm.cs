namespace WebStrategy.ModelConvert
{
    public class Worm
    {
        public string name { get; set; }
        public int lifeStrength { get; set; }
        public Position position { get; set; }
    }
}