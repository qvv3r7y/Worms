using System.Collections.Generic;

namespace WebStrategy.ModelConvert
{
    public class Field
    {
        public List<Worm> worms { get; set; }
        public List<Food> food { get; set; }
    }
}