using System.Collections.Generic;
using WebStrategy.Model;
using WebStrategy.Strategy;

namespace WebStrategy.ModelConvert
{
    public class Converter
    {
        public static Field Old2New(World world)
        {
            var result = new Field { food = new List<Food>(), worms = new List<Worm>()};
            foreach (var oldFood in world.GetFoods())
            {
                result.food.Add(new Food
                {
                    position = new Position {x = oldFood.Coords.x, y = oldFood.Coords.y},
                    expiresIn = oldFood.Freshness
                });
            }
            foreach (var oldWorm in world.GetWorms())
            {
                result.worms.Add(new Worm()
                {
                    position = new Position {x = oldWorm.Coords.x, y = oldWorm.Coords.y},
                    name = oldWorm.GetName(),
                    lifeStrength = oldWorm.GetHealth()
                });
            }

            return result;
        }
        
        public static World New2Old(Field field, int step)
        {
            var result = new World();
            result.StepCounter = step;
            foreach (var food in field.food)
            {
                var oldFood = new Model.Food(food.position.x, food.position.y);
                oldFood.Freshness = food.expiresIn;
                result.TryAddNewFood(oldFood);
            }
            foreach (var worm in field.worms)
            {
                var oldWorm = new Model.Worm(new UpStrategy(), result, worm.position.x, worm.position.y);
                oldWorm.Health = worm.lifeStrength;
                oldWorm.Name = worm.name;
                result.AddNewWorm(oldWorm);
            }

            return result;
        }
    }
}