using WebStrategy.Strategy;

namespace WebStrategy.Model
{
    public class StepWeb
    {
        public string direction { get; set; }
        public bool split { get; set; }

        public StepWeb(Direction direction, bool split)
        {
            this.direction = direction.ToString();
            this.split = split;
        }
    }
}