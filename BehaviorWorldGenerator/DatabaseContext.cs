using BehaviorWorldGenerator.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BehaviorWorldGenerator
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Behavior> Behaviors { get; set; }
        public DbSet<Entity.Food> Foods { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BehaviorConfiguration());
            modelBuilder.ApplyConfiguration(new FoodConfiguration());
        }
        
        public class BehaviorConfiguration : IEntityTypeConfiguration<Behavior>
        {
            public void Configure(EntityTypeBuilder<Behavior> builder)
            {
                builder.ToTable("behavior");
                builder.Property(b => b.BehaviorId).HasColumnName("behavior_id");
                builder.Property(b => b.Name).HasColumnName("name").IsRequired().HasMaxLength(128);
            }
        }

        public class FoodConfiguration : IEntityTypeConfiguration<Entity.Food>
        {
            public void Configure(EntityTypeBuilder<Entity.Food> builder)
            {
                builder.ToTable("food");
                builder.Property(f => f.FoodId).HasColumnName("food_id");
                builder.Property(f => f.BehaviorId).HasColumnName("behavior_id").IsRequired();
                builder.Property(f => f.Step).HasColumnName("step").IsRequired();
                builder.Property(f => f.X).HasColumnName("x").IsRequired();
                builder.Property(f => f.Y).HasColumnName("y").IsRequired();
            }
        }
    }
}