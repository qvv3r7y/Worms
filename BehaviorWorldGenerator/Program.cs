﻿using System;
using BehaviorWorldGenerator.Food.Generation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BehaviorWorldGenerator
{
    public static class Program
    {
        private const string ConnectionString = @"Server=localhost;Port=5432;Database=food;Username=food;Password=food";
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            var behaviorName = args.Length != 0 ? args[0] : "Test";

            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService(sp =>
                        new BehaviorService(
                            sp.GetService<IHostApplicationLifetime>(),
                            sp.GetService<DatabaseContext>(),
                            sp.GetService<IFoodGenerator>(),
                            behaviorName));
                    services.AddDbContext<DatabaseContext>(op=> op.UseNpgsql(ConnectionString));
                    services.AddScoped<IFoodGenerator, NormalFoodGenerator>();
                });
        }
    }
}