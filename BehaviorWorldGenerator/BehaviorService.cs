using System;
using System.Threading;
using System.Threading.Tasks;
using BehaviorWorldGenerator.Food.Generation;
using Microsoft.Extensions.Hosting;

namespace BehaviorWorldGenerator
{
    public class BehaviorService : IHostedService
    {
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly DatabaseContext _dbContext;
        private readonly IFoodGenerator _foodGenerator;
        private readonly string _behaviorName;

        public BehaviorService(IHostApplicationLifetime appLifetime, DatabaseContext dbContext,
            IFoodGenerator foodGenerator, string behaviorName)
        {
            _appLifetime = appLifetime;
            _dbContext = dbContext;
            _foodGenerator = foodGenerator;
            _behaviorName = behaviorName;
        }

        private void WriteBehavior()
        {
            _dbContext.Database.EnsureCreated();

            var behavior = new Entity.Behavior() {Name = _behaviorName};
            _dbContext.Behaviors.Add(behavior);
            _dbContext.SaveChanges();

            var foodList = _foodGenerator.GenerateFood();
            var step = 0;
            foreach (var (x, y) in foodList)
            {
                var food = new Entity.Food()
                {
                    BehaviorId = behavior.BehaviorId,
                    X = x,
                    Y = y,
                    Step = ++step
                };
                _dbContext.Foods.Add(food);
            }

            _dbContext.SaveChanges();
            Console.WriteLine("Поведение сгенерировано и записано в БД");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _appLifetime.ApplicationStarted.Register(() =>
            {
                WriteBehavior();
                _appLifetime.StopApplication();
            });
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}