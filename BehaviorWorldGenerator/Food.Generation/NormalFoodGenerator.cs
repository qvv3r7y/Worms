using System;
using System.Collections.Generic;
using System.Linq;
using BehaviorWorldGenerator.Utils;

namespace BehaviorWorldGenerator.Food.Generation
{
    public class NormalFoodGenerator : IFoodGenerator
    {
        private const int NumSteps = 100;

        public List<(int, int)> GenerateFood()
        {
            var random = new Random();
            var foodCoords = new List<(int, int)>();
            while (foodCoords.Count != NumSteps)
            {
                var x = random.NextNormal();
                var y = random.NextNormal();
                foodCoords.Add((x, y));
                if (foodCoords.Count == NumSteps)
                {
                    foodCoords = foodCoords.Distinct().ToList();
                }
            }

            return foodCoords;
        }
    }
}