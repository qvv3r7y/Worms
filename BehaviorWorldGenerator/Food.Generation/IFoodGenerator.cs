using System.Collections.Generic;

namespace BehaviorWorldGenerator.Food.Generation
{
    public interface IFoodGenerator
    {
        List<(int, int)> GenerateFood();
    }
}