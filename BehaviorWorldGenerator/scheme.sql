drop table food;
drop table behavior;

create table behavior (
    behavior_id SERIAL       primary key,
    name        varchar(128) UNIQUE NOT NULL
);

create table food (
    food_id     SERIAL primary key,
    behavior_id int,
    step        int NOT NULL,
    x           int NOT NULL,
    y           int NOT NULL,
    UNIQUE (behavior_id, step),
    FOREIGN KEY (behavior_id) REFERENCES behavior (behavior_id)
);

select * from behavior;
select step, x, y from behavior bh left join food b on bh.behavior_id = b.behavior_id order by step;
